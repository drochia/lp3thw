formatter = "{} {} {} {}"

print(formatter.format(1, 2, 3, 4))                     # using the format method the values from the brackets
print(formatter.format("one", "two", "three", "four"))  # are placed in the curly brackets of the formatter
print(formatter.format(True, False, False, True))
print(formatter.format(formatter, formatter, formatter, formatter))  # 4 curly brackets pairs are placed into each of the 4 curly brackets pair
print(formatter.format(
    "Rain drops keep falling on my head,",
    "And just like the guy who's feet are too big for his bed,",
    "Nothing seems to fit. Yeah!",
    "Rain drops keep falling on my head, they keep falling."
))
