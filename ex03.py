# a message will be printed
print("I will now count my chickens:")

# messages printed and the results of arithmetic calculations
print("Hens", 25.0 + 30.0 / 6.0)
print("Roosters", 100.0 - 25.0 * 3.0 % 4.0)

# the phrase from the quotes will be printed
print("Now I will count the eggs:")

# the result of calculations between the parentheses will be printed
print(3.0 + 2.0 + 1.0 - 5.0 + 4.0 % 2.0 - 1.0 / 4.0 + 6.0)

# the message from the quotes will be printed
print("Is it true that 3 + 2 < 5 - 7?")

# math calculation performed, the results compared(bool)  and printed
print(3.0 + 2.0 < 5.0 - 7.0)

# messages printed, math calculations performed and the results printed
print("What is 3 + 2?", 3.0 + 2.0)
print("What is 5 - 7?", 5.0 - 7.0)

# message printed
print("Oh, that's why it's False.")

# message printed
print("How about some more.")

# print the messages, compare (bool) and print the results
print("Is it greater?", 5.0 > -2.0)
print("Is it greater or equal?", 5.0 >= -2.0)
print("Is it less or equal?", 5.0 <= -2.0)


