# import unicodedata as ud

b_slash = "\\"
s_quote = "\'"
d_quote = "\""
a_bell = "\a"
a_b_space = "\b"
a_formfeed = "\f"
a_linefeed = "\n"
# c_name = "Ю"
c_return = "\r"
h_tab = "\t"
v_tab = "\v"

print("This is a backslash:", b_slash)
print("This is a single quote:", s_quote, "and this is a double quote:", d_quote)

# for i in range(10):
print("This is the ASCII bell:", a_bell)
#    i += 1

print("This is the ASCII backspace:" + a_b_space * 5)
print("This is the ASCII formfeed:", a_formfeed)
print("This is the ASCII linefeed:", a_linefeed)

# l_name = print(ud.name(c_name))
# print('\N{l_name}')
print("This is a Carriage return:", c_return)
print(h_tab, "This is a Horizontal tab.")
print(v_tab, "This is a Vertical tab.")
