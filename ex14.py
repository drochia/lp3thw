from sys import argv

script, user_name, country = argv
prompt = '\t\_ '

print(f"\nHi {user_name}, I'm the {script} script.")
print("I'd like to ask you a few questions.")
print(f"\n\tDo you like me {user_name}?")
likes = input(prompt)

print(f"\n\tWhere do you live {user_name}?")
lives = input(prompt)

print("\n\tWhat kind of computer do you have?")
computer = input(prompt)

print(f"""
Alright, so you said {likes} about liking me.
You live in {lives}, {country}.  Not sure where that is.
And you have a {computer} computer.  Nice.
""")
