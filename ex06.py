types_of_people = 10  # assigning the value 10 to the variable
x = f"There are {types_of_people} types of people."  #  assigning a formated string to variable x  //-> string including strings


binary = "binary"  # assigning the string between the brackets to the variable
do_not = "don't"  # same as above
y = f"Those who know {binary} and those who {do_not}."  # assigning a formated string to variable y  //-> string including strings


print(x)  # prints the content of x variable
print(y)  # prints the content of y variable

print(f"I said: {x}")  # prints a string including the string from variable 'x'
print(f"I also said: '{y}'")  # prints a string including the string from variable 'y'

hilarious = False  # assigns boolean value False to the variable
joke_evaluation = "Isn't that joke so funny?! {}"  # assings a formated string to the variable

print(joke_evaluation.format(hilarious))  # prints the value of the variable using the 'format' method containing the value of another variable

w = "This is the left side of..."  # assign the string from the double quotes to variable 'w'
e = "a string with a right side."  # assign the string from the double quotes to variable 'e'

print(w + e)  # prints the values of variables 'w' and 'e' in one string (concatenates them)
