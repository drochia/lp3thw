from sys import argv
#  read the WYSS section for how to run this
script, first, second, third = argv

print("The script is called:", script)
print("Your first variable is:", first)
print("Your second variable is:", second)
print("Your third variable is:", third)
print(argv)
print(argv[0])

in_weight = input("Please enter your weight in kilograms: ")
in_height = input("Please enter your height in centimeters: ")

print(f"So, you are {in_weight} kg heavy and {in_height} cm tall.")
