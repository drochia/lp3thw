from sys import argv			# imports argv from sys module

script, filename = argv			# assigns the CLI arguments to argv

txt = open(filename)			# assigns the open instruction to txt variable

print(f"Here's your file {filename}:")	# prints a formatted string/message
print(txt.read())			# prints the output of the txt.read() \
txt.close()				# opens the file assigned to txt variable in read mode and prints its content
print("Type the filename again:")	# prints a string/message
file_again = input("> ")		# assigns to file_again variable the string passed to input prompt

txt_again = open(file_again)		# assigns the open command to txt_again variable

print(txt_again.read())			# opens the file in read mode and print its content
txt_again.close()			# closing the file
