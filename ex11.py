print("How old are you?", end=' ')
age = input("\t")
print("How tall are you?", end=' ')
height = input("\t")
print("How much do you weigh?", end=' ')
weight = input("\t")

print(f"\v\tSo, you're {age} old, {height} tall and {weight} heavy.\n")

city = input("What city do you live in? \t")
country = input("What country do you live? \t")
continent = input("What continent do you live? \t")

print(f"\v\tSo you live in {city}, located in {country} and is part of {continent}.")
