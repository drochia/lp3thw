print("Mary had a little lamb.")
print("Its fleece was white as {}.".format('snow'))  # using the .format method to insert the value into the formated string
print("And everywhere that Mary went.")
print("." * 10)  # this line will print 10 consecutive dots.

end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"


print(end1 + end2 + end3 + end4 + end5 + end6, end=' ')  # the (, end=' ') replaces the (\n) new line with a space and
print(end7 + end8 + end9 + end10 + end11 + end12)        # Burger prints on the same line as the Cheese
